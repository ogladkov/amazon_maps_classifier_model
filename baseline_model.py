import os
import cv2
import numpy as np
from glob import glob
import json
import pandas as pd
from omegaconf import OmegaConf  # noqa
from clearml import Task
import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping
import torch
import torch.nn.functional as F
import torch.nn as nn
from torchmetrics.functional import f1_score, fbeta_score, precision, recall
import timm
from argparse import ArgumentParser
from dataloader import DataModule, get_test_transforms


class PlanetModel(pl.LightningModule):
    def __init__(self, cfg):
        super().__init__()
        self.cfg = cfg
        self.efficient_net = timm.create_model(self.cfg['model']['name'], pretrained=True)
        n_features = self.efficient_net.classifier.in_features
        self.efficient_net.classifier = nn.Sequential(
            nn.Linear(n_features, n_features // 2),
            nn.ReLU(),
            nn.Linear(n_features // 2, self.cfg['model']['num_classes']),
            nn.Sigmoid(),
        )

    def forward(self, x): # noqa
        return self.efficient_net(x)

    def configure_optimizers(self): # noqa
        optimizer = torch.optim.Adam(self.parameters(), lr=self.cfg['model']['lr']) # noqa
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=2, gamma=0.1)
        return [optimizer], [scheduler]

    def training_step(self, batch, batch_idx): # noqa
        x, y = batch['images'], batch['labels']
        x = x.type(torch.float32)
        y = y.type(torch.float32)
        y_hat = self(x)
        loss = F.binary_cross_entropy(y_hat, y)
        fbeta = fbeta_score(y_hat.type(torch.int32), y.type(torch.int32),
                            beta=2,
                            # num_classes=self.cfg['model']['num_classes'],
                            mdmc_average='samplewise',
                            average='samples',
                            multiclass=True)
        self.log('train_loss', loss, on_step=True, on_epoch=True, prog_bar=True, logger=True)
        self.log("train_fbeta", fbeta, on_epoch=True, prog_bar=False, logger=True)
        return loss

    def validation_step(self, batch, batch_idx): # noqa
        x, y = batch['images'], batch['labels']
        x = x.type(torch.float32)
        y = y.type(torch.float32)
        y_hat = self(x)
        loss = F.binary_cross_entropy(y_hat, y)
        fbeta = fbeta_score(y_hat.type(torch.int32), y.type(torch.int32),
                            beta=2,
                            # num_classes=self.cfg['model']['num_classes'],
                            mdmc_average='samplewise',
                            average='samples',
                            multiclass=True)
        self.log('val_loss', loss, on_epoch=True, prog_bar=True, logger=True)
        self.log("val_fbeta", fbeta, on_epoch=True, prog_bar=False, logger=True)
        return loss

    def predict_step(self, batch, batch_idx, dataloader_idx=0):
        return self(batch)


if __name__ == '__main__':
    # ArgParser
    parser = ArgumentParser()
    parser.add_argument("--action", type=str, default='test', choices=['test', 'train'])
    args = parser.parse_args()

    # Config
    cfg = OmegaConf.load('config.yml')

    if args.action == 'train':
        task = Task.init(
            project_name='amazon_maps_classifier_model',
            task_name=f'{cfg["model"]["name"]} with FBeta metric',
        )

        for fold in range(cfg['model']['cv_folds']):
            dm = DataModule(cfg, fold)
            dm.prepare_data()
            dm.setup()

            planet_model = PlanetModel(cfg)
            model_checkpoint = ModelCheckpoint(
                monitor='val_loss',
                verbose=True,
                filename='{fold}_{epoch}_{val_loss:.4f}',
            )
            early_stopping = EarlyStopping('val_loss', patience=4)
            trainer = pl.Trainer(
                gpus=1,
                max_epochs=cfg['model']['num_epochs'],
                callbacks=[model_checkpoint, early_stopping],
                fast_dev_run=False,
            )
            trainer.fit(planet_model, dm)

    elif args.action == 'test':
        out = np.empty((
            len(os.listdir('images/input')),
            cfg['model']['num_classes'],
            cfg['model']['cv_folds']
             ))

        for w_num, w in enumerate(glob('best_models/*.ckpt')):
            planet_model = PlanetModel(cfg)
            planet_model = planet_model.load_from_checkpoint(cfg=cfg, checkpoint_path=w)

            dm = DataModule(cfg)
            dm.prepare_data()
            dm.setup(stage='test')

            for b_num, batch in enumerate(iter(dm.test_dataloader())):
                batch = batch['images']
                preds = planet_model(batch)
                out[b_num:b_num+cfg['model']['batch_size'], :, w_num] = preds.detach()

        out = ((out.sum(axis=2) / cfg['model']['cv_folds']).round().astype(np.int32))

        categories = dm.df.columns[1:]
        all_out_cats = []
        for y in out:
            iter_out_cats = []
            for n, x in enumerate(y):
                if x == 1:
                    iter_out_cats.append(categories[n])
            all_out_cats.append(iter_out_cats)

        # all_out_cats = pd.DataFrame(all_out_cats)
        out_dict = {}
        for n, x in enumerate(all_out_cats):
            out_dict[dm.test_df.reset_index(drop=True).iloc[n]['image_name']] = x

        with open('out.json', 'w') as j:
            json.dump(out_dict, j)
