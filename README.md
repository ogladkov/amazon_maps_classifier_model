# AMAZON CLASSIFICATION MODEL

## Installation
* Clone this repository: 'git clone https://gitlab.com/ogladkov/amazon_maps_classifier_model.git'
* Install requirements and corresponding PyTorch (CPU or GPU): 'pip install -r requirements.txt'

## Training
* Edit 'config.yml'
* Run training with 'python3 baseline_model.py --action train'

## Inference
* Install DVC: 'pip install dvc[ssh]'
* Set up DVC:
  * dvc remote add -f ssh-storage ssh://ogladkov@91.206.15.25/home/ogladkov/hw01/dvc
  * dvc remote modify ssh-storage user ogladkov
  * dvc remote modify ssh-storage port 22
  * dvc remote modify ssh-storage password "password"
* Pull DVC files: 'dvc pull'
* Put your images to 'images/input' folder
* Run 'python3 baseline_model.py --action test'
* Get a result in 'out.json'

## Metrics and training results
* Training ClearML dashboard is [here](https://app.clear.ml/projects/8a218b31033644fd8ffe4a2b1d770b69/experiments/b503281acd434eb780647d66a168f6ad/output/execution)