import pytest
from omegaconf import OmegaConf


@pytest.fixture
def cfg():
    return OmegaConf.load('config.yml')

@pytest.fixture
def fold():
    return 0
