import os
import cv2
import pandas as pd
import numpy as np
from typing import Optional
from tqdm import tqdm
import pickle
from iterstrat.ml_stratifiers import MultilabelStratifiedKFold
import pytorch_lightning as pl
from torch.utils.data import Dataset, random_split, DataLoader, Subset
from albumentations.pytorch.transforms import ToTensorV2
import albumentations as A
from omegaconf import OmegaConf  # noqa


def get_train_transforms(cfg):
        return A.Compose([A.Transpose(p=0.5),
                            A.RandomRotate90(p=1),
                            A.HorizontalFlip(p=0.5),
                            A.VerticalFlip(p=0.5),
                            A.HueSaturationValue(p=0.5, hue_shift_limit=5, sat_shift_limit=40, val_shift_limit=15),
                            A.RandomBrightnessContrast(p=0.5, brightness_limit=0.3),
                            A.Normalize(cfg['model']['preprocessing']['mean'],
                                        cfg['model']['preprocessing']['std'],
                                        max_pixel_value=255, always_apply=True),
                            A.CoarseDropout(p=0.01, max_width=20, max_height=20),
                            ToTensorV2(always_apply=True)
                            ])


def get_val_transforms(cfg):
    return A.Compose([A.Normalize(cfg['model']['preprocessing']['mean'],
                    cfg['model']['preprocessing']['std'],
                    max_pixel_value=255, always_apply=True),
                      ToTensorV2(always_apply=True)
                      ])

def get_test_transforms(cfg):
    return A.Compose([A.Normalize(cfg['model']['preprocessing']['mean'],
                      cfg['model']['preprocessing']['std'],
                      max_pixel_value=255, always_apply=True),
                      ToTensorV2(always_apply=True)
                      ])

class PlanetDataset(Dataset):
    def __init__(self, df, transforms=None):
        self.df = df
        self.img_names = df['image_name'].values
        self.labels = df.iloc[:, 1:].values
        self.transforms = transforms

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        img_path = self.img_names[idx]
        img = cv2.imread(img_path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        label = self.labels[idx]

        if self.transforms:
            img = self.transforms(image=img)['image']

        return {'images': img, 'labels': label}


class DataModule(pl.LightningDataModule):
    def __init__(self, cfg, fold=None):
        super().__init__()
        self.cfg = cfg
        self.fold = fold
        self.csv_path = self.cfg['paths']['csv_path']
        self.jpgs_path = self.cfg['paths']['train_jpgs_path']
        self.batch_size = self.cfg['model']['batch_size']
        self.t_transforms = get_train_transforms(self.cfg)
        self.v_transforms = get_val_transforms(self.cfg)
        self.tt_transforms = get_test_transforms(self.cfg)

    def prepare_data(self):
        if 'df.pickle' not in os.listdir('.'):
            self.df = pd.read_csv(self.csv_path)
            self.df.tags = self.df.tags.str.split()
            categories = list(self.df.tags)
            categories = set([item for sublist in categories for item in sublist])

            for cat in categories:
                self.df.loc[:, cat] = 0

            for x in tqdm(range(self.df.shape[0])):
                for tag in self.df.iloc[x]['tags']:
                    if tag in self.df.columns:
                        self.df.loc[x, tag] += 1

            self.df.drop('tags', axis=1, inplace=True)
            self.df['image_name'] = self.jpgs_path + '/' + self.df['image_name'] + '.jpg'

            with open('df.pickle', 'wb') as file_df:
                pickle.dump(self.df, file_df)

        else:
            with open('df.pickle', 'rb') as file_df:
                self.df = pickle.load(file_df)

    def setup(self, stage: Optional[str] = None):
        if stage == "fit" or stage is None:
            self.full = PlanetDataset(self.df, transforms=self.t_transforms)
            mskf = MultilabelStratifiedKFold(n_splits=self.cfg['model']['cv_folds'],
                                             shuffle=True,
                                             random_state=self.cfg['seeds']['mskf'])
            all_splits = [k for k in mskf.split(self.df.iloc[:, 0], self.df.iloc[:, 1:])]
            train_indexes, val_indexes = all_splits[self.fold]
            train_indexes, val_indexes = train_indexes.tolist(), val_indexes.tolist()

            self.train = Subset(self.full, train_indexes)
            self.val = Subset(self.full, val_indexes)
        elif stage == 'test':
            self.test_df = pd.DataFrame(os.listdir('./images/input'), columns=['image_name'])
            self.test_df = self.test_df.sort_values('image_name')
            self.test_df['image_name'] = './images/input/' + self.test_df['image_name']
            self.test = PlanetDataset(self.test_df, transforms=self.tt_transforms)

    def train_dataloader(self):
        return DataLoader(self.train, batch_size=self.cfg['model']['batch_size'],
                          shuffle=False, num_workers=os.cpu_count())

    def val_dataloader(self):
        return DataLoader(self.val, batch_size=self.cfg['model']['batch_size'],
                          shuffle=False, num_workers=os.cpu_count())

    def test_dataloader(self):
        return DataLoader(self.test, batch_size=self.cfg['model']['batch_size'],
                          shuffle=False, num_workers=os.cpu_count())


if __name__ == '__main__':
    cfg = OmegaConf.load('config.yml')
    dm = DataModule(cfg)
    dm.prepare_data()
    dm.setup(stage='test')
    print(next(iter(dm.test_dataloader()))['images'].dtype)