import torch
from amazon_maps_classifier_model.dataloader import DataModule
from omegaconf import OmegaConf  # noqa
import os


cfg = OmegaConf.load('config.yml')

def test_dataloader():
    dm = DataModule(cfg)
    dm.prepare_data()
    dm.setup(stage='test')
    assert next(iter(dm.test_dataloader()))['images'].dtype == torch.float32