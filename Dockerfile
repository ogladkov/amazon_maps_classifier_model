FROM ubuntu:latest

RUN apt update
RUN apt install -y docker.io
RUN apt install -y python3-pip
RUN apt install -y git
RUN pip3 install torch torchvision --extra-index-url https://download.pytorch.org/whl/cpu
RUN git clone https://gitlab.com/ogladkov/amazon_maps_classifier_model.git
RUN pip3 install -r amazon_maps_classifier_model/requirements.txt
RUN cd amazon_maps_classifier_model/
