import os
import shutil
from omegaconf import OmegaConf


def transfer_weights(cfg):
    for f in os.listdir(cfg['paths']['best_weights']):
        f = os.path.join(cfg['paths']['best_weights'], f)
        os.remove(f)

    versions = os.listdir(cfg['paths']['pl_weights'])
    versions = [x.lstrip('version_').zfill(5) for x in versions]
    versions.sort(reverse=True)
    last_folds = versions[:cfg['model']['cv_folds']]
    last_folds = ['version_' + str(int(x)) for x in last_folds]
    weight_paths = [os.path.join(cfg['paths']['pl_weights'], x, 'checkpoints') for x in last_folds]
    weight_paths = [os.path.join(x, os.listdir(x)[-1]) for x in weight_paths]
    dst_path = cfg['paths']['best_weights']

    for x in weight_paths:
        shutil.copy2(x, dst_path)


if __name__ == '__main__':
    cfg = OmegaConf.load('config.yml')
    transfer_weights(cfg)